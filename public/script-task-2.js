
let result2 = 0;
let answer2 = [];
let seconds2;

function startTest2(id_title, id_subtitle, btn) {
    document.getElementById(id_title).style.display="none";
    document.getElementById(id_subtitle).style.display="none";
    btn.style.display='none';
    answer2 = [1, 2, 3, 4, 5];
    shuffle(answer2);

    for (let i = 0; i < 5; i++){
        let id = "num" + (i+1);
        document.getElementById(id).innerHTML = answer2[i];
    }
    startTimer();
}

let number1;
function chooseNumber(number){
    number.classList.toggle('chosenNumber');
    if (number1 == null) number1 = number;
    else {
        let i = number1.id[3] - 1;
        let j = number.id[3] - 1;
        let t = answer2[i];
        answer2[i] = answer2[j];
        answer2[j] = t;
        result2++;
        document.getElementById(number1.id).innerHTML = answer2[i];
        document.getElementById(number.id).innerHTML = answer2[j];

        number1.classList.toggle('chosenNumber');
        number.classList.toggle('chosenNumber');
        number1 = null;
    }
    if (isArrayOrdered(answer2)) finishTest2();
}

function finishTest2() {
    stopTimer();
    seconds2 = seconds;
    seconds = 0;
    document.getElementById('test2').style.display="none";
    document.getElementById('result2').style.display="flex";
    document.getElementById('resOut_2').innerHTML = result2.toString();
    document.getElementById('time_2').innerHTML = seconds2;

    data.test_2.answer = answer2;
    data.test_2.result = result2;
    data.test_2.time = seconds2;
}

function isArrayOrdered(array) {
    for (let i = 0; i < array.length - 1; i++)
        if (array[i] > array[i+1]) return false;
    return true;
}
