
let result3 = [];
let answer3 = [];
let seconds3;

let colors = [ ["Зеленый", "red"], ["Красный", "blue"], ["Синий", "pink"], ["Розовый", "rgb(255,215,0)"], ["Желтый", "green"] ];
let currentColor = 0;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function startTest3(id_title, id_subtitle, btn) {
    document.getElementById(id_title).style.display="none";
    document.getElementById(id_subtitle).style.display="none";
    btn.style.display='none';
    document.getElementById("test-3-round").style.display="flex";

    currentColor = getRandomInt(4)
    setRound();

    startTimer();
}

function chooseColor(btn) {
    let text_answer = document.getElementById(btn).textContent;
    answer3.push(colors[(currentColor + 1) % 5][0]);
    result3.push(text_answer);
    currentColor = (currentColor + 1) % 5;

    if (answer3.length === 5) finishTest3();
    else setRound();
}
function setRound() {

    document.getElementById("test-3-word").innerHTML = colors[currentColor][0];
    document.getElementById("test-3-word").style.color = colors[currentColor][1];

    let id_answer = "color" + (getRandomInt(3) + 1);
    document.getElementById(id_answer).innerHTML = colors[(currentColor + 1) % 5][0];

    for (let i = 1; i <= 3; i++) {
        let id = "color" + i;
        if (id !== id_answer) document.getElementById(id).innerHTML = colors[(currentColor + i + 1) % 5][0];
        document.getElementById(id).style.color = colors[(currentColor + i + 1) % 5][1];
        document.getElementById(id).style.backgroundColor = colors[(currentColor + i + 3) % 5][1];
    }

}

function finishTest3() {
    stopTimer();
    seconds3 = seconds;
    seconds = 0;
    document.getElementById('test3').style.display="none";
    document.getElementById('result3').style.display="flex";

    let correct = 0;
     for (let i = 0; i < 5; i++){
         if (answer3[i] === result3[i]) correct++;
     }
    document.getElementById('resOut_3').innerHTML = correct.toString();
    document.getElementById('time_3').innerHTML = seconds3;

    data.test_3.answer = answer3;
    data.test_3.result = result3;
    data.test_3.time = seconds3;

    console.log(data)
}

